## Edit batch file

You would need to grab **withdrawNdelegate.sh** and alter it by setting your wallet address, password for your wallet and validator address. Additionally you may modify other variables on the top.

*Important*
In PATH you should set the path where your rizond is located. For example: /home/alex/go/bin/

By default scripts waits 10 seconds after withdrawing rewards and before delegating them. You can end up with error if you won't make this pause.

## Set up cron job

You can edit your user's cron file by running:

*crontab -e*

Your task may look like:
\*/10 * * * *        /home/alex/scripts/withdrawNdelegage.sh >> /home/alex/scripts/run.log 2>&1

In this example task would be executed each 10 minutes and write output, including errors to run.log

You can view your cron tasks by running:

*crontab -l*

To check cron logs you can use:

*grep -i cron /var/log/syslog*