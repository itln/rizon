#!/bin/bash
PATH=PATH_TO_RIZON:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

password='YOUR_PASSWORD'
from_addr='YOUR_WALLET'
chain='groot-07'
fees='20uatolo'
delay_sec=10
validator_addr='rizonvaloper1vwml3z2k9yx9zczwgkmr8ea2qf6tj4m9w7tyk8'

echo $password | rizond tx distribution withdraw-rewards $validator_addr \
--from $from_addr \
--chain-id=$chain \
--fees=$fees \
--commission -y

sleep $delay_sec

balance=$(rizond query bank balances $from_addr -o json | jq -r '.balances | .[].amount');
balance=$(($balance-50))

echo $password | rizond tx staking delegate $validator_addr ${balance}uatolo \
--from $from_addr \
--chain-id $chain \
--fees=$fees -y